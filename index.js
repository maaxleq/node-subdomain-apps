const vhost = require("vhost")
const connect = require("connect")
const fs = require("fs")
const path = require("path")

function middleware(domainName, appSubdomainsFile, appsDirectory){
    let connector = connect()

    let appSubdomains = JSON.parse(fs.readFileSync(appSubdomainsFile))

    for (let subdomain in appSubdomains){
        let fullDomain

        if (subdomain === ""){
            fullDomain = domainName
        }
        else {
            fullDomain = subdomain + "." + domainName
        }

        let app = require(path.join(appsDirectory, appSubdomains[subdomain], "index"))

        connector.use(vhost(fullDomain, app))
    }

    return connector
}

module.exports = middleware